#! /bin/sh
# Script to flash images via fastboot, edit image path first

sudo ./fastboot flash ram /path/to/u-boot-with-spl.bin
sudo ./fastboot reboot
sleep 10
sudo ./fastboot flash uboot /path/to/u-boot-with-spl.bin
sudo ./fastboot flash boot /path/to/boot.ext4
sudo ./fastboot flash root /path/to/rootfs.ext4
