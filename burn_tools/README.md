
# burn_tools

This is the tools used for flashing images onto LicheePi 4A boards.

It is available from https://dl.sipeed.com/shareURL/LICHEE/licheepi4a/07_Tools.

## Usage

1. Edit burn_lpi4a.sh

Edit image path of u-boot-with-spl.bin, boot.ext4 and rootfs.ext4.

2. Enter burn mode

To enter burn mode, press BOOT button on the borad and reinsert the USB-C cable.

3. Run burn_lpi4a.sh

Run the following command to burn:

	$ sh burn_lpi4a.sh

## Images

### U-Boot

Available as ../uboot/u-boot-with-spl.bin

The process of building u-boot-with-spl.bin can be referenced in ../uboot/README.md.

### rootfs

You can built rootfs.ext4 with the buildroot config ../configs/buildroot_2023.02.2_defconfig.

There are 1 more change on the rootfs made by buildroot.

Cyclictest is added for real-time testing, which is available by:

	$ git clone git://git.kernel.org/pub/scm/utils/rt-tests/rt-tests.git
	$ cd rt-tests
	$ git checkout stable/v1.0
	$ make cyclictest

The CPIO version of rootfs (including cyclictest) is also available at ../root/2023.02.2/rootfs.cpio.gz.

### boot.ext4

The size of boot.ext4 is 60MB, it should be created with the following files:

* fw_dynamic.bin

OpenSBI, available as ../bios/opensbi/generic/fw_dynamic.bin

The process of building it can be referenced in ../bios/opensbi/README.md.

* Image

Kernel image, available as ../kernel/v6.5/Image

* th1520-lichee-pi-4a.dtb

Device tree blob, available as ../kernel/v6.5/th1520-lichee-pi-4a.dtb

* extlinux/extlinux.conf

Configuration file needed by U-Boot

Command for reference to create this:

	$ cd /path/to/mounted/rootfs
	$ mkdir extlinux
	$ touch extlinux.conf
	$ echo "label Linux-rt
	$ 	kernel /Image
	$ 	fdt /th1520-lichee-pi-4a.dtb
	$ 	append  console=ttyS0,115200 root=/dev/mmcblk0p3 rootfstype=ext4 rootwait rw 
	$	earlycon rootrwoptions=rw,noatime rootrwreset=yes init=/sbin/init" > extlinux.conf

## Reference

* [Sipeed LicheePi 4A Documentation](https://wiki.sipeed.com/hardware/zh/lichee/th1520/lpi4a/4_burn_image.html)
