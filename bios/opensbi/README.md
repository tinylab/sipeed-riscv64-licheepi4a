
# OpenSBI

This OpenSBI is for T-Head SoCs.

The source code is available from https://github.com/xhackerustc/thead-opensbi.

Currently used commit id: ea45aaa3d6a5 ("lib: utils: reset: thead: add back "plic-delegate" handling")

You can compile it with the command:

	$ git clone https://github.com/xhackerustc/thead-opensbi opensbi
	$ cd opensbi
	$ make PLATFORM=generic ARCH=riscv CROSS_COMPILE=${your_cross_compiling_toolchain} 
	$ ls build/platform/generic/firmware/fw_dynamic.bin

