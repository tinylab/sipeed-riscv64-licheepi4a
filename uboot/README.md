
# U-Boot

This U-Boot is for T-Head SoCs.

The source code is available from https://github.com/revyos/thead-u-boot.

Currently used commit id: 329e2581fe5b ("Linux_SDK_V1.2.1")

You can compile it with the command:

	$ git clone https://github.com/revyos/thead-u-boot uboot
	$ cd uboot
	$ git switch lpi4a

	# for board with 8G memory
	$ make light_lpi4a_defconfig

	# for board with 16G memory
	#$ make light_lpi4a_16g_defconfig

	$ make CROSS_COMPILE=${your_cross_compiling_toolchain} ARCH=riscv
	$ find . -name "u-boot-with-spl.bin"
