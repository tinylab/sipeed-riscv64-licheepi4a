
# sipeed riscv64/licheepi4a board bsp

This is for [Linux Lab](https://tinylab.org/linux-lab).

## Use it in Linux Lab

    $ cd /path/to/linux-lab
    $ make B=riscv64/licheepi4a
    $ make kernel
